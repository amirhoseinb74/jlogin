package com.example.jlogin.presentation.tasks

import com.example.jlogin.data.remote.dto.Task

data class TasksState(
    val loading: Boolean = false,
    val data: List<Task>? = null
)
