package com.example.jlogin.presentation.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jlogin.data.remote.dto.ApiResult
import com.example.jlogin.domain.use_case.LoginUseCase
import com.example.jlogin.domain.use_case.StoreUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase,
    private val storeUserUseCase: StoreUserUseCase
) : ViewModel() {

    private var loginJob: Job? = null

    private val _loadingState = MutableStateFlow(false)
    val loadingState: StateFlow<Boolean> = _loadingState

    private val _eventChannel = Channel<LoginUIEvent>()
    val eventChannel = _eventChannel.receiveAsFlow()

    fun onEvent(event: LoginEvent) {
        when (event) {
            is LoginEvent.LoginClicked -> {
                login(username = event.username)
            }
        }
    }

    private fun login(username: String) {
        loginJob?.cancel()
        loginJob = loginUseCase(username).onEach { apiResult ->
            when (apiResult) {
                is ApiResult.Error -> {
                    _loadingState.update { false }
                    showErrorInUI(apiResult.message!!)
                }

                is ApiResult.Loading -> {
                    _loadingState.update { true }
                }

                is ApiResult.Success -> {
                    _loadingState.update { false }
                    storeUserUseCase(apiResult.data!!)
                    _eventChannel.send(LoginUIEvent.NavigateToHome)
                }
            }
        }.launchIn(viewModelScope)
    }

    private suspend fun showErrorInUI(error: String) {
        _eventChannel.send(LoginUIEvent.ShowError(error))
    }

    override fun onCleared() {
        loginJob?.cancel()
        super.onCleared()
    }

    sealed interface LoginUIEvent {
        data object NavigateToHome : LoginUIEvent
        data class ShowError(val message: String) : LoginUIEvent
    }
}
