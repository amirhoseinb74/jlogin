package com.example.jlogin.presentation.util

sealed class Screen(val route: String) {
    data object Login : Screen("login_screen")
    data object Home : Screen("home_screen")
    data object Tasks : Screen("tasks_screen")
    data object Profile : Screen("profile_screen")
}