package com.example.jlogin.presentation.home

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import com.example.jlogin.presentation.profile.ProfileScreen
import com.example.jlogin.presentation.tasks.TasksScreen
import com.example.jlogin.presentation.util.Screen
import com.example.jlogin.presentation.util.sharedViewModel

fun NavGraphBuilder.homeNavigation(navController: NavHostController) {
    navigation(
        route = Screen.Home.route,
        startDestination = Screen.Tasks.route
    ) {
        composable(Screen.Tasks.route) {
            val viewModel: HomeViewModel =
                it.sharedViewModel(navController = navController)
            TasksScreen(navController = navController, viewModel = viewModel)
        }
        composable(Screen.Profile.route) {
            val viewModel: HomeViewModel =
                it.sharedViewModel(navController = navController)
            ProfileScreen(navController = navController, viewModel = viewModel)
        }
    }
}