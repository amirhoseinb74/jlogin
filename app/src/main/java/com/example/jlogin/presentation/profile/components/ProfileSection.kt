package com.example.jlogin.presentation.profile.components

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.jlogin.data.remote.dto.Address
import com.example.jlogin.data.remote.dto.Company
import com.example.jlogin.data.remote.dto.Geo
import com.example.jlogin.data.remote.dto.User
import com.example.jlogin.presentation.core.ui.theme.JLoginTheme

@Composable
fun ProfileSection(user: User) {
    Surface(
        modifier = Modifier
            .fillMaxWidth(),
        shape = RoundedCornerShape(16.dp)
    ) {
        val scrollState = rememberScrollState()
        Column(
            modifier = Modifier
                .padding(16.dp)
                .verticalScroll(scrollState)
        ) {
            ProfileItem("Name", user.name)
            ProfileItem("Username", user.username)
            ProfileItem("Email", user.email)
            ProfileItem("Phone", user.phone)
            ProfileItem("Website", user.website)
            ProfileItem("Company", user.company.name)
            ProfileItem("Catch Phrase", user.company.catchPhrase)
            ProfileItem("Business", user.company.bs)
            ProfileItem(
                "Address",
                "${user.address.street}, ${user.address.suite}, ${user.address.city}, ${user.address.zipcode}"
            )
            val context = LocalContext.current

            Button(modifier = Modifier.align(Alignment.CenterHorizontally), onClick = {
                showMapAction(context, user.address.geo.lat, user.address.geo.lng)
            }) {
                Text("Show User Location on Map")
            }
        }
    }
}

private fun showMapAction(context: Context, latitudeString: String, longitudeString: String) {
    runCatching {
        val latitude = latitudeString.toDouble()
        val longitude = longitudeString.toDouble()
        Intent(Intent.ACTION_VIEW, Uri.parse("geo:$latitude,$longitude")).apply {
            resolveActivity(context.packageManager)?.let {
                context.startActivity(this)
            } ?: Toast.makeText(context, "No map app installed", Toast.LENGTH_SHORT).show()
        }
    }.onFailure {
        Toast.makeText(context, "Invalid latitude or longitude format", Toast.LENGTH_SHORT).show()
    }
}

@Preview
@Composable
private fun ProfileSectionPreview() {
    JLoginTheme {
        ProfileSection(
            user = User(
                address = Address(
                    "Tehran",
                    geo = Geo("", ""),
                    street = "Mollasadra",
                    suite = "124",
                    "346214123"
                ),
                company = Company("be", "cached..", "nameOfCompany"),
                email = "example@something.com",
                id = 3,
                name = "Name of User",
                phone = "phone number",
                username = "username here",
                website = "mybesite.org"
            )
        )
    }
}