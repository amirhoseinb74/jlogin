package com.example.jlogin.presentation.profile.components

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.example.jlogin.R

@Composable
fun LogoutDialog(
    onLogoutConfirmed: () -> Unit,
    onDialogDismissed: () -> Unit
) {
    AlertDialog(
        onDismissRequest = { onDialogDismissed() },
        title = {
            Text(text = stringResource(id = R.string.logout_title))
        },
        text = {
            Text(text = stringResource(id = R.string.logout_message))
        },
        confirmButton = {
            Button(
                onClick = {
                    onLogoutConfirmed()
                    onDialogDismissed()
                }
            ) {
                Text(text = stringResource(id = R.string.logout_confirm))
            }
        },
        dismissButton = {
            Button(
                onClick = { onDialogDismissed() }
            ) {
                Text(text = stringResource(id = R.string.cancel))
            }
        }
    )
}
