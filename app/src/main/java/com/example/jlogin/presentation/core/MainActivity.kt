package com.example.jlogin.presentation.core

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.jlogin.presentation.core.components.LoadingAnimation
import com.example.jlogin.presentation.core.ui.theme.JLoginTheme
import com.example.jlogin.presentation.home.homeNavigation
import com.example.jlogin.presentation.login.LoginScreen
import com.example.jlogin.presentation.util.Screen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            JLoginTheme {
                val navController = rememberNavController()

                val viewModel: MainViewModel = hiltViewModel()
                val ready by viewModel.isReady.collectAsState()
                val startDestination by viewModel.startDestination.collectAsState()

                if (ready) {
                    NavHost(
                        navController = navController,
                        startDestination = startDestination
                    ) {
                        composable(Screen.Login.route) {
                            LoginScreen(navController)
                        }
                        homeNavigation(navController)
                    }
                } else {
                    LoadingAnimation()
                }
            }
        }
    }
}