package com.example.jlogin.presentation.tasks

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.example.jlogin.R
import com.example.jlogin.presentation.core.components.LoadingAnimation
import com.example.jlogin.presentation.home.HomeEvent
import com.example.jlogin.presentation.home.HomeViewModel
import com.example.jlogin.presentation.home.components.HomeScaffold
import com.example.jlogin.presentation.tasks.components.SearchBar
import com.example.jlogin.presentation.tasks.components.TaskItem
import com.example.jlogin.presentation.tasks.components.TasksBackHandler

@Composable
fun TasksScreen(
    navController: NavController, viewModel: HomeViewModel
) {

    val searchedText by viewModel.searchText.collectAsState()
    HomeScaffold(
        navController = navController,
        title = stringResource(R.string.tasks),
        topBarActions = {
            SearchBar(
                text = searchedText,
                onTextChange = viewModel::onSearchTextChange,
                onSearchClick = viewModel::onSearchTextChange
            )
        },
        homeUIEvent = viewModel.eventChannel
    ) {
        val taskState by viewModel.taskState.collectAsState()
        LazyColumn {
            taskState.data?.let { tasks ->
                items(tasks) { task ->
                    TaskItem(task = task)
                }
            }
        }

        TasksBackHandler(onLogout = { viewModel.onEvent(HomeEvent.Logout) })

        if (taskState.loading)
            LoadingAnimation()
    }
}