package com.example.jlogin.presentation.home.components.bottom_sheet

import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBarItem
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.jlogin.presentation.core.ui.theme.JLoginTheme

@Composable
fun BottomNavigation(
    navController: NavController
) {

    val items = listOf(
        BottomNavItem.Tasks,
        BottomNavItem.UserInfo
    )

    val contentColor = MaterialTheme.colorScheme.primaryContainer

    BottomAppBar(
        contentColor = contentColor
    ) {
        items.forEach { item ->
            NavigationBarItem(
                selected = item.selected,
                onClick = {
                    if (!item.selected) {
                        navController.navigate(item.direction)
                    }
                },
                icon = {
                    Icon(imageVector = item.icon, contentDescription = item.title)
                }
            )
        }
    }

    navController.addOnDestinationChangedListener { _, destination, _ ->
        items.forEach { item ->
            item.selected = item.direction == destination.route
        }
    }
}

@Preview
@Composable
fun BottomNavigationPreview() {
    JLoginTheme {
        BottomNavigation(navController = rememberNavController())
    }
}
