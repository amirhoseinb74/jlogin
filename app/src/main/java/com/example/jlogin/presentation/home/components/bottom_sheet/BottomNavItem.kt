package com.example.jlogin.presentation.home.components.bottom_sheet

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Checklist
import androidx.compose.material.icons.filled.Person
import androidx.compose.ui.graphics.vector.ImageVector
import com.example.jlogin.presentation.util.Screen

sealed class BottomNavItem(
    var title: String,
    var direction: String,
    var icon: ImageVector,
    var selected: Boolean = false,
) {
    data object Tasks : BottomNavItem(
        title = "Tasks",
        direction = Screen.Tasks.route,
        icon = Icons.Default.Checklist,
        selected = true
    )

    data object UserInfo : BottomNavItem(
        title = "Profile",
        direction = Screen.Profile.route,
        icon = Icons.Default.Person
    )
}
