package com.example.jlogin.presentation.home.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.example.jlogin.presentation.home.HomeViewModel
import com.example.jlogin.presentation.home.components.bottom_sheet.BottomNavigation
import com.example.jlogin.presentation.util.ObserveAsEvents
import com.example.jlogin.presentation.util.Screen
import kotlinx.coroutines.flow.Flow

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScaffold(
    modifier: Modifier = Modifier,
    navController: NavController,
    title: String,
    homeUIEvent: Flow<HomeViewModel.HomeUIEvent>,
    topBarActions: @Composable RowScope.() -> Unit = {},
    content: @Composable () -> Unit
) {

    val snackbarHostState = remember { SnackbarHostState() }

    homeUIEvent.ObserveAsEvents { event ->
        when (event) {
            is HomeViewModel.HomeUIEvent.ShowError -> {
                snackbarHostState.showSnackbar(message = event.message)
            }

            is HomeViewModel.HomeUIEvent.NavigateToLogin -> {
                navController.navigate(Screen.Login.route) {
                    popUpTo(Screen.Home.route) {
                        inclusive = true
                    }
                    launchSingleTop = true
                }
            }
        }
    }

    Scaffold(
        modifier = modifier,
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
        bottomBar = { BottomNavigation(navController = navController) },
        topBar = {
            TopAppBar(title = { Text(text = title) }, actions = topBarActions)
        }) { innerPadding ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(innerPadding)
        ) {
            content()
        }
    }
}