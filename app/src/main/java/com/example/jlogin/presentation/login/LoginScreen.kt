package com.example.jlogin.presentation.login


import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.SoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.jlogin.R
import com.example.jlogin.presentation.core.components.LoadingAnimation
import com.example.jlogin.presentation.core.ui.theme.JLoginTheme
import com.example.jlogin.presentation.util.ObserveAsEvents
import com.example.jlogin.presentation.util.Screen

@Composable
fun LoginScreen(
    navController: NavController,
    viewModel: LoginViewModel = hiltViewModel()
) {
    val loadingState by viewModel.loadingState.collectAsState()
    val snackbarHostState = remember { SnackbarHostState() }

    viewModel.eventChannel.ObserveAsEvents { event ->
        when (event) {
            LoginViewModel.LoginUIEvent.NavigateToHome -> navController.navigate(Screen.Home.route)
            is LoginViewModel.LoginUIEvent.ShowError -> {
                snackbarHostState.showSnackbar(message = event.message)
            }
        }
    }

    LoginContent(
        handleEvent = viewModel::onEvent,
        loading = loadingState,
        snackbarHostState = snackbarHostState
    )
}

@Composable
fun LoginContent(
    handleEvent: (LoginEvent) -> Unit,
    loading: Boolean,
    snackbarHostState: SnackbarHostState
) {
    var username by rememberSaveable { mutableStateOf("") }
    val keyboardController = LocalSoftwareKeyboardController.current

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(innerPadding)
                .padding(16.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            OutlinedTextField(
                value = username,
                onValueChange = { username = it },
                label = { Text(stringResource(R.string.username)) },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(onDone = {
                    onClickLogin(keyboardController, handleEvent, username)
                }),
            )

            Spacer(modifier = Modifier.height(16.dp))

            Button(
                onClick = { onClickLogin(keyboardController, handleEvent, username) },
                enabled = username.isNotBlank()
            ) {
                Text(stringResource(R.string.login))
            }
        }
        if (loading)
            LoadingAnimation()
    }
}

private fun onClickLogin(
    keyboardController: SoftwareKeyboardController?,
    handleEvent: (LoginEvent) -> Unit,
    username: String
) {
    keyboardController?.hide()
    handleEvent(LoginEvent.LoginClicked(username))
}

@Preview(showBackground = true)
@Composable
fun LoginPreview() {
    JLoginTheme {
        LoginContent(
            handleEvent = {},
            loading = false,
            snackbarHostState = SnackbarHostState()
        )
    }
}