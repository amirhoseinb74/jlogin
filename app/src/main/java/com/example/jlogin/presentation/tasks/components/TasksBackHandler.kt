package com.example.jlogin.presentation.tasks.components

import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext

@Composable
fun TasksBackHandler(
    onLogout: () -> Unit
) {
    val context = LocalContext.current
    var showExitOrLogoutDialog by remember {
        mutableStateOf(false)
    }

    if (showExitOrLogoutDialog) {
        ExitOrLogoutDialog(
            onExitClick = {
                val activity = (context as? ComponentActivity)
                activity?.finishAffinity()
            },
            onLogoutClick = onLogout,
            onDismiss = { showExitOrLogoutDialog = false }
        )
    }

    BackHandler(enabled = true) {
        showExitOrLogoutDialog = true
    }
}