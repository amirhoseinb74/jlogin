package com.example.jlogin.presentation.home

sealed interface HomeEvent {
    data object Logout : HomeEvent
}