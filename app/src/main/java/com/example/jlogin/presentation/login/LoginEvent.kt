package com.example.jlogin.presentation.login

sealed interface LoginEvent {
    data class LoginClicked(val username: String) : LoginEvent
}