package com.example.jlogin.presentation.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jlogin.data.remote.dto.ApiResult
import com.example.jlogin.data.remote.dto.User
import com.example.jlogin.domain.use_case.GetStoredUserUseCase
import com.example.jlogin.domain.use_case.GetTasksUseCase
import com.example.jlogin.domain.use_case.LogOutUseCase
import com.example.jlogin.presentation.tasks.TasksState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getTasksUseCase: GetTasksUseCase,
    private val getStoredUserUseCase: GetStoredUserUseCase,
    private val logOutUseCase: LogOutUseCase
) : ViewModel() {

    private var getTasksJob: Job? = null

    private val _user = MutableStateFlow<User?>(null)
    val user: StateFlow<User?> = _user

    private val _searchText = MutableStateFlow("")
    val searchText = _searchText.asStateFlow()

    private val _taskState = MutableStateFlow(TasksState())
    val taskState: StateFlow<TasksState> = searchText
        .debounce(500L)
        .combine(_taskState) { text, taskState ->
            if (text.isBlank())
                taskState
            else
                taskState.copy(data = taskState.data?.filter {
                    it.title.contains(
                        text,
                        ignoreCase = true
                    )
                })
        }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), TasksState())

    fun onSearchTextChange(text: String) {
        _searchText.value = text
    }

    private val _eventChannel = Channel<HomeUIEvent>()
    val eventChannel = _eventChannel.receiveAsFlow()

    init {
        initializeUser()
        getUserTasks()
    }

    fun onEvent(event: HomeEvent) {
        when (event) {
            is HomeEvent.Logout -> viewModelScope.launch {
                logOutUseCase()
                _eventChannel.send(HomeUIEvent.NavigateToLogin)
            }
        }
    }

    private fun initializeUser() {
        viewModelScope.launch {
            getStoredUserUseCase()?.let { user ->
                _user.update { user }
            }
        }
    }

    private fun getUserTasks() {
        getTasksJob?.cancel()
        user.value?.id?.let { userId ->
            fetchTasks(userId)
        }
    }

    private fun fetchTasks(userId: Int) {
        getTasksJob = getTasksUseCase(userId)
            .onEach { apiResult ->
                _taskState.update {
                    when (apiResult) {
                        is ApiResult.Error -> {
                            showErrorInUI(apiResult.message!!)
                            TasksState()
                        }

                        is ApiResult.Loading -> {
                            TasksState(loading = true)
                        }

                        is ApiResult.Success -> {
                            TasksState(data = apiResult.data)
                        }
                    }
                }
            }
            .launchIn(viewModelScope)
    }

    private suspend fun showErrorInUI(error: String) {
        _eventChannel.send(HomeUIEvent.ShowError(error))
    }

    override fun onCleared() {
        getTasksJob?.cancel()
        super.onCleared()
    }

    sealed interface HomeUIEvent {
        data class ShowError(val message: String) : HomeUIEvent
        data object NavigateToLogin : HomeUIEvent
    }
}
