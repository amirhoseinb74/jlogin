package com.example.jlogin.presentation.profile

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.Logout
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.example.jlogin.R
import com.example.jlogin.presentation.home.HomeEvent
import com.example.jlogin.presentation.home.HomeViewModel
import com.example.jlogin.presentation.home.components.HomeScaffold
import com.example.jlogin.presentation.profile.components.LogoutDialog
import com.example.jlogin.presentation.profile.components.ProfileSection

@Composable
fun ProfileScreen(
    navController: NavController,
    viewModel: HomeViewModel
) {

    val user by viewModel.user.collectAsState()
    var showLogoutDialog by remember { mutableStateOf(false) }

    HomeScaffold(
        navController = navController,
        title = stringResource(R.string.profile),
        topBarActions = {
            IconButton(onClick = { showLogoutDialog = true }) {
                Icon(
                    imageVector = Icons.AutoMirrored.Outlined.Logout,
                    contentDescription = stringResource(id = R.string.logout)
                )
            }
        },
        homeUIEvent = viewModel.eventChannel
    ) {
        user?.let {
            ProfileSection(user = it)
        } ?: Text(text = stringResource(R.string.can_not_find_user_data))

        if (showLogoutDialog) {
            LogoutDialog(
                onLogoutConfirmed = { viewModel.onEvent(HomeEvent.Logout) },
                onDialogDismissed = { showLogoutDialog = false })
        }
    }
}