package com.example.jlogin.presentation.tasks.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.example.jlogin.R
import com.example.jlogin.presentation.core.ui.theme.JLoginTheme

@Composable
fun ExitOrLogoutDialog(
    onExitClick: () -> Unit,
    onLogoutClick: () -> Unit,
    onDismiss: () -> Unit,
) {
    val dialogBgColor = MaterialTheme.colorScheme.surface
    val onDialogColor = MaterialTheme.colorScheme.onSurface

    Dialog(
        onDismissRequest = { onDismiss() }
    ) {
        Box(modifier = Modifier.fillMaxSize()) {

            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(16.dp)
                    .align(Alignment.BottomCenter),
                colors = CardDefaults.cardColors(
                    containerColor = dialogBgColor,
                    contentColor = onDialogColor
                ),
                elevation = CardDefaults.cardElevation(12.dp)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    Text(
                        textAlign = TextAlign.Start,
                        text = stringResource(R.string.what_would_you_like_to_do),
                        style = MaterialTheme.typography.bodyMedium,
                        modifier = Modifier
                            .padding(bottom = 12.dp)
                            .align(Alignment.CenterHorizontally)
                    )

                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Absolute.SpaceEvenly
                    ) {
                        TextButton(
                            colors = ButtonColors(
                                containerColor = MaterialTheme.colorScheme.tertiary,
                                contentColor = MaterialTheme.colorScheme.onTertiary,
                                disabledContentColor = Color.Unspecified,
                                disabledContainerColor = Color.Unspecified
                            ),
                            onClick = {
                                onExitClick()
                                onDismiss()
                            }) {
                            Text(text = stringResource(R.string.exit))
                        }
                        TextButton(
                            colors = ButtonColors(
                                containerColor = MaterialTheme.colorScheme.primary,
                                contentColor = MaterialTheme.colorScheme.onPrimary,
                                disabledContentColor = Color.Unspecified,
                                disabledContainerColor = Color.Unspecified
                            ),
                            onClick = {
                                onLogoutClick()
                                onDismiss()
                            }) {
                            Text(text = stringResource(R.string.logout))
                        }
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ExitOrLogoutDialogPreview() {
    JLoginTheme {
        ExitOrLogoutDialog(onExitClick = {}, onLogoutClick = {}, onDismiss = {})
    }
}