package com.example.jlogin.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.example.jlogin.data.remote.AppApi
import com.example.jlogin.data.repository.LoginRepositoryImpl
import com.example.jlogin.data.repository.TaskRepositoryImpl
import com.example.jlogin.data.repository.UserRepositoryImpl
import com.example.jlogin.domain.repository.LoginRepository
import com.example.jlogin.domain.repository.TaskRepository
import com.example.jlogin.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideAppApi(retrofit: Retrofit): AppApi = retrofit.create(AppApi::class.java)

    @Provides
    @Singleton
    fun provideLoginRepository(api: AppApi): LoginRepository = LoginRepositoryImpl(api)

    @Provides
    @Singleton
    fun provideUserRepository(
        dataStore: DataStore<Preferences>
    ): UserRepository = UserRepositoryImpl(dataStore)

    private val Context.userDataStore: DataStore<Preferences> by preferencesDataStore(
        name = "com.example.jlogin.preferences"
    )

    @Provides
    @Singleton
    fun provideDataStorePreferences(@ApplicationContext applicationContext: Context): DataStore<Preferences> =
        applicationContext.userDataStore


    @Provides
    @Singleton
    fun provideTaskRepository(api: AppApi): TaskRepository = TaskRepositoryImpl(api)
}