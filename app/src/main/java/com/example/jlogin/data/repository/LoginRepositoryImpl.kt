package com.example.jlogin.data.repository

import com.example.jlogin.data.remote.AppApi
import com.example.jlogin.data.remote.dto.User
import com.example.jlogin.domain.repository.LoginRepository

class LoginRepositoryImpl(
    private val api: AppApi,
) : LoginRepository {

    override suspend fun login(username: String): List<User> {
        return api.loginUser(username)
    }
}