package com.example.jlogin.data.repository

import com.example.jlogin.data.remote.AppApi
import com.example.jlogin.data.remote.dto.Task
import com.example.jlogin.domain.repository.TaskRepository

class TaskRepositoryImpl(
    private val api: AppApi
) : TaskRepository {

    override suspend fun getTasks(userId: Int): List<Task> {
        return api.getUserTasks(userId)
    }
}
