package com.example.jlogin.data.util

object DataConstants {
    const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    const val NETWORK_TIMEOUT = 60L //in seconds
}
