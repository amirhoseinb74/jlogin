package com.example.jlogin.data.remote.dto

data class Task(
    val completed: Boolean,
    val id: Int,
    val title: String,
    val userId: Int
)