package com.example.jlogin.data.remote.dto

data class Geo(
    val lat: String,
    val lng: String
)