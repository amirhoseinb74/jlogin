package com.example.jlogin.data.repository

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import com.example.jlogin.data.remote.AppApi
import com.example.jlogin.data.remote.dto.User
import com.example.jlogin.domain.repository.UserRepository
import com.google.gson.Gson
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map
import java.io.IOException

class UserRepositoryImpl(
    private val dataStore: DataStore<Preferences>
) : UserRepository {

    private companion object {
        private val KEY_USER = stringPreferencesKey(name = "user")
    }

    override suspend fun storeUser(user: User) {
        val userJson = Gson().toJson(user)
        dataStore.edit { preferences ->
            preferences[KEY_USER] = userJson
        }
    }

    override suspend fun getStoredUser(): User? {
        val storedUserJson = dataStore.data.catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map { preferences ->
            preferences[KEY_USER]
        }.firstOrNull()

        return storedUserJson?.let { Gson().fromJson(storedUserJson, User::class.java) }
    }

    override suspend fun removeStoredUser() {
        dataStore.edit { preferences ->
            preferences.remove(KEY_USER)
        }
    }
}
