package com.example.jlogin.data.remote

import com.example.jlogin.data.remote.dto.Task
import com.example.jlogin.data.remote.dto.User
import retrofit2.http.GET
import retrofit2.http.Query

interface AppApi {

    @GET("users")
    suspend fun loginUser(
        @Query("username") username: String
    ): List<User>

    @GET("todos")
    suspend fun getUserTasks(
        @Query("userId") userId: Int
    ): List<Task>
}