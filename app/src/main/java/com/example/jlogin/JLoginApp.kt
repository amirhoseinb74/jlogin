package com.example.jlogin

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class JLoginApp : Application()