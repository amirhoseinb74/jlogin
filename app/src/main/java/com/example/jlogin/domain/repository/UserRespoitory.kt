package com.example.jlogin.domain.repository

import com.example.jlogin.data.remote.dto.User

interface UserRepository {
    suspend fun storeUser(user: User)
    suspend fun getStoredUser(): User?
    suspend fun removeStoredUser()
}
