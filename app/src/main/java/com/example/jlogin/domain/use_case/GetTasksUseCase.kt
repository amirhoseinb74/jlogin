package com.example.jlogin.domain.use_case

import com.example.jlogin.data.remote.dto.ApiResult
import com.example.jlogin.domain.repository.TaskRepository
import com.example.jlogin.domain.util.handleExceptions
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetTasksUseCase @Inject constructor(
    private val repository: TaskRepository
) {
    operator fun invoke(userId: Int) = flow {
        emit(ApiResult.Loading())
        val tasks = repository.getTasks(userId)
        emit(ApiResult.Success(tasks))
    }.handleExceptions()
}
