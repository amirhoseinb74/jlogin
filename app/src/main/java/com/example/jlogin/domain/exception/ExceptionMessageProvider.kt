package com.example.jlogin.domain.exception

object ExceptionMessageProvider {
    const val HTTP_EXCEPTION_MESSAGE = "An unexpected error occurred"
    const val IO_EXCEPTION_MESSAGE = "Couldn't reach the server. Check your internet connection."
    const val UNKNOWN_EXCEPTION_MESSAGE = "Unknown error!"
}