package com.example.jlogin.domain.util

import com.example.jlogin.data.remote.dto.ApiResult
import com.example.jlogin.domain.exception.ExceptionMessageProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import retrofit2.HttpException
import java.io.IOException

fun <T> Flow<ApiResult<T>>.handleExceptions() = catch { e ->
    val errorMessage = when (e) {
        is HttpException -> ExceptionMessageProvider.HTTP_EXCEPTION_MESSAGE
        is IOException -> ExceptionMessageProvider.IO_EXCEPTION_MESSAGE
        else -> e.localizedMessage ?: ExceptionMessageProvider.UNKNOWN_EXCEPTION_MESSAGE
    }
    emit(ApiResult.Error(errorMessage))
}
