package com.example.jlogin.domain.use_case

import com.example.jlogin.data.remote.dto.ApiResult
import com.example.jlogin.domain.exception.UserNotFoundException
import com.example.jlogin.domain.repository.LoginRepository
import com.example.jlogin.domain.util.handleExceptions
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val repository: LoginRepository
) {
    operator fun invoke(username: String) = flow {
        emit(ApiResult.Loading())
        val users = repository.login(username)

        if (users.isEmpty()) throw UserNotFoundException()
        val user = users[0]
        emit(ApiResult.Success(user))
    }.handleExceptions()
}
