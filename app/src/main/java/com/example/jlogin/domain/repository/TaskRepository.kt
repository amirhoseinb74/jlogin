package com.example.jlogin.domain.repository

import com.example.jlogin.data.remote.dto.Task

interface TaskRepository {

    suspend fun getTasks(userId: Int): List<Task>
}