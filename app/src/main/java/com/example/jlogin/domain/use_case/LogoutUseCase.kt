package com.example.jlogin.domain.use_case

import com.example.jlogin.domain.repository.UserRepository
import javax.inject.Inject

class LogOutUseCase @Inject constructor(
    private val repository: UserRepository
) {
    suspend operator fun invoke() {
        repository.removeStoredUser()
    }
}
