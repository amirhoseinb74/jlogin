package com.example.jlogin.domain.use_case


import com.example.jlogin.data.remote.dto.User
import com.example.jlogin.domain.repository.UserRepository
import javax.inject.Inject

class GetStoredUserUseCase @Inject constructor(
    private val repository: UserRepository
) {
    suspend operator fun invoke(): User? {
        return repository.getStoredUser()
    }
}