package com.example.jlogin.domain.repository

import com.example.jlogin.data.remote.dto.User

interface LoginRepository {
    suspend fun login(username: String): List<User>
}