package com.example.jlogin.domain.exception

class UserNotFoundException : Exception("User Not Found")