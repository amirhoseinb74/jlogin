package com.example.jlogin.domain.use_case

import com.example.jlogin.data.remote.AppApi
import com.example.jlogin.data.remote.dto.ApiResult
import com.example.jlogin.data.remote.dto.Task
import com.example.jlogin.data.repository.TaskRepositoryImpl
import com.example.jlogin.domain.exception.ExceptionMessageProvider
import com.example.jlogin.domain.repository.TaskRepository
import com.example.jlogin.domain.util.readJsonFileFromTestResources
import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GetTasksUseCaseTest {

    private val mockWebServer = MockWebServer()
    private val client = OkHttpClient.Builder().build()
    private lateinit var getTasksUseCase: GetTasksUseCase
    private lateinit var repository: TaskRepository
    private lateinit var apiService: AppApi

    @Before
    fun setUp() {
        apiService = Retrofit.Builder().baseUrl(mockWebServer.url("/")).client(client)
            .addConverterFactory(GsonConverterFactory.create()).build().create(AppApi::class.java)

        repository = TaskRepositoryImpl(apiService)
        getTasksUseCase = GetTasksUseCase(repository)
    }

    @Test
    fun `fetch user todos, success with correct data`() = runTest {

        val userId = 1
        val jsonResponseBody =
            javaClass.classLoader?.readJsonFileFromTestResources("sample_tasks.json")
        val response = MockResponse().setResponseCode(200).setBody(jsonResponseBody!!)

        mockWebServer.enqueue(response)
        val result = getTasksUseCase(userId).toList()

        val loadingResult = result[0]
        val successResult = result[1]
        assertThat(loadingResult).isInstanceOf(ApiResult.Loading::class.java)
        assertThat(successResult).isInstanceOf(ApiResult.Success::class.java)

        val jsonResponse = response.getBody()?.readUtf8()
        val tasks: List<Task> =
            Gson().fromJson(jsonResponse, object : TypeToken<List<Task>>() {}.type)

        assertThat(successResult.data).isEqualTo(tasks)
    }

    @Test
    fun `fetch user todos, network error`() = runTest {
        val userId = 1

        mockWebServer.shutdown()
        val result = getTasksUseCase(userId).toList()

        val loadingResult = result[0]
        val errorResult = result[1]
        assertThat(loadingResult).isInstanceOf(ApiResult.Loading::class.java)
        assertThat(errorResult).isInstanceOf(ApiResult.Error::class.java)
        assertThat(errorResult.message).isEqualTo(ExceptionMessageProvider.IO_EXCEPTION_MESSAGE)
    }

    @Test
    fun `fetch user todos, http error`() = runTest {
        val userId = 1

        val response = MockResponse().setResponseCode(400)
        mockWebServer.enqueue(response)

        val result = getTasksUseCase(userId).toList()

        val loadingResult = result[0]
        val errorResult = result[1]
        assertThat(loadingResult).isInstanceOf(ApiResult.Loading::class.java)
        assertThat(errorResult).isInstanceOf(ApiResult.Error::class.java)
        assertThat(errorResult.message).isEqualTo(ExceptionMessageProvider.HTTP_EXCEPTION_MESSAGE)
    }
}