package com.example.jlogin.domain.util

import org.junit.Assert.assertNotNull
import java.io.File
import java.nio.file.Paths

fun ClassLoader.readJsonFileFromTestResources(fileName: String): String {
    val resource = getResource(fileName)
    assertNotNull("Resource $fileName could not be found.", resource)
    val file = File(Paths.get(resource.toURI()).toString())
    return file.readText(Charsets.UTF_8)
}

