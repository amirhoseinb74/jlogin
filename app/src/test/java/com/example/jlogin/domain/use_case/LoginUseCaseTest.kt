package com.example.jlogin.domain.use_case

import com.example.jlogin.data.remote.AppApi
import com.example.jlogin.data.remote.dto.ApiResult
import com.example.jlogin.data.remote.dto.User
import com.example.jlogin.data.repository.LoginRepositoryImpl
import com.example.jlogin.domain.exception.ExceptionMessageProvider
import com.example.jlogin.domain.exception.UserNotFoundException
import com.example.jlogin.domain.repository.LoginRepository
import com.example.jlogin.domain.util.readJsonFileFromTestResources
import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@ExperimentalCoroutinesApi
class LoginUseCaseTest {

    private val mockWebServer = MockWebServer()
    private val client = OkHttpClient.Builder().build()
    private lateinit var loginUseCase: LoginUseCase
    private lateinit var repository: LoginRepository
    private lateinit var apiService: AppApi

    @Before
    fun setUp() {
        apiService = Retrofit.Builder().baseUrl(mockWebServer.url("/")).client(client)
            .addConverterFactory(GsonConverterFactory.create()).build().create(AppApi::class.java)

        repository = LoginRepositoryImpl(apiService)
        loginUseCase = LoginUseCase(repository)
    }

    @Test
    fun `login existing user, success with correct data`() = runTest {
        val username = "Bret"
        val jsonResponseBody =
            javaClass.classLoader?.readJsonFileFromTestResources("sample_user.json")
        val response = MockResponse().setResponseCode(200).setBody(jsonResponseBody!!)
        mockWebServer.enqueue(response)
        val result = loginUseCase(username).toList()

        val loadingResult = result[0]
        val successResult = result[1]
        assertThat(loadingResult).isInstanceOf(ApiResult.Loading::class.java)
        assertThat(successResult).isInstanceOf(ApiResult.Success::class.java)
        assertThat(successResult.data?.username).isEqualTo(username)

        val jsonResponse = response.getBody()?.readUtf8()
        val users: List<User> =
            Gson().fromJson(jsonResponse, object : TypeToken<List<User>>() {}.type)
        assertThat(successResult.data).isEqualTo(users.first())
    }

    @Test
    fun `login non-existing user, error with correct message`() = runTest {
        val username = "InvalidUser"
        val response = MockResponse().setResponseCode(200).setBody("[]")
        mockWebServer.enqueue(response)

        val result = loginUseCase(username).toList()

        val loadingResult = result[0]
        val failureResult = result[1]
        assertThat(loadingResult).isInstanceOf(ApiResult.Loading::class.java)
        assertThat(failureResult).isInstanceOf(ApiResult.Error::class.java)
        assertThat(failureResult.message).isEqualTo(UserNotFoundException().message)
    }

    @Test
    fun `login user, network error`() = runTest {
        val username = "anything"
        mockWebServer.shutdown()

        val result = loginUseCase(username).toList()

        val loadingResult = result[0]
        val errorResult = result[1]
        assertThat(loadingResult).isInstanceOf(ApiResult.Loading::class.java)
        assertThat(errorResult).isInstanceOf(ApiResult.Error::class.java)
        assertThat(errorResult.message).isEqualTo(ExceptionMessageProvider.IO_EXCEPTION_MESSAGE)
    }

    @Test
    fun `login user, http error`() = runTest {
        val username = "anything"

        val response = MockResponse().setResponseCode(400)
        mockWebServer.enqueue(response)

        val result = loginUseCase(username).toList()

        val loadingResult = result[0]
        val errorResult = result[1]
        assertThat(loadingResult).isInstanceOf(ApiResult.Loading::class.java)
        assertThat(errorResult).isInstanceOf(ApiResult.Error::class.java)
        assertThat(errorResult.message).isEqualTo(ExceptionMessageProvider.HTTP_EXCEPTION_MESSAGE)
    }
}